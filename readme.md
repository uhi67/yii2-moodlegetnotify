Moodle Get Notify
=================

Yii2 client class to use the mdl-getnot API.

Prerequisites
-------------

    yii2 >= 2.0.13
    php >= 7.2

Installation
------------

The preferred way to install this extension is through composer.

To install, either run

    composer require uhi67/yii2-moodlegetnotify "*" 

or add

    "uhi67/yii2-moodlegetnotify" : "*"

or clone form the repository

    git clone git@bitbucket.org:uhi67/yii2-moodlegetnotify.git

Usage
-----

### Settings in config file (web.php)


    'components' => [
        ...
        'moodleGetNotify' => [
            'class' => \uhi67\moodlegetnotify\MoodleGetNotify::class,
			'url' => null, // insert actual API url here, e.g. 'https://learning.educ.test/mdl-getnot/web/get_messages.php'
			'secret' => null, // insert the API shared secret here
		],
    ]
    
### Usage in the controller

This code fragment gets the number of notifications of the logged in user in the Moodle.  

    $saml = 
	if($saml && $saml->isAuthenticated()) {
		$moodleNotifications = Yii::$app->moodleNotifier->getNot(ArrayHelper::getValue($saml->get('eduPersonPrincipalName'), 0));
