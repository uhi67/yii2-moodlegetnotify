<?php /** @noinspection PhpUnused */


namespace uhi67\moodlegetnotify;


use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class MoodleNotifier
 *
 * Client for mdl-getnot API
 *
 * @package app\components
 */
class MoodleGetNotify {
	/** @var string $url -- url of the webservice */
	public $url;
	/** @var string $secret -- shared secret required to generate the one-time-token */
	public $secret;

	/**
	 * @throws \Exception
	 */
	public function getNot($uid) {
		$ts = time();

		$query = [
			'uid' => $uid,
			'ts' => $ts,
			'token' => hash('sha512', "$uid,$ts,$this->secret"),
		];
		$response = self::get_fcontent($this->url.'?'.http_build_query($query));
		$mdl_response = json_decode($response, JSON_OBJECT_AS_ARRAY);
		if(!is_array($mdl_response) || ArrayHelper::getValue($mdl_response, 'status')!='success') {
			Yii::error('Moodle notifier failed. user='.$uid.', error='.ArrayHelper::getValue($mdl_response, 'error'));
			return 0;
		}
		return ArrayHelper::getValue($mdl_response, 'notifications',0)+ArrayHelper::getValue($mdl_response, 'messages',0)+ArrayHelper::getValue($mdl_response, 'requests',0);
	}

	/** @noinspection PhpSameParameterValueInspection */
	private static function get_fcontent($url, $timeout = 5) {
		$url = str_replace( "&amp;", "&", urldecode(trim($url)) );

		$cookie = tempnam ("/tmp", "CURLCOOKIE");
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_ENCODING, "" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
		curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
		curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
		$content = curl_exec( $ch );
		$response = curl_getinfo( $ch );
		curl_close ( $ch );

		if($response['http_code'] == 301 || $response['http_code'] == 302) {
			ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");

			if ( $headers = get_headers($response['url'])) {
				foreach( $headers as $value) {
					if ( substr(strtolower($value), 0, 9) == "location:")
						return self::get_fcontent(trim(substr($value, 9, strlen($value))));
				}
			}
		}

		return $content;
	}
}
